import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mrsmoothiespanel/Screens/ProductsScreen.dart';
import 'package:mrsmoothiespanel/app/controller/routes.dart';
import 'package:mrsmoothiespanel/data/model/Product.dart';
import 'package:mrsmoothiespanel/app/theme.dart';

import 'Screens/HomeScreen.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(const MyApp());
}

Size medias(BuildContext context) {
  return MediaQuery.of(context).size;
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      initialRoute: HomePage.routeName,
      title: 'Flutter Demo',
      theme: theme(),
      getPages: appRoutes(),
    );
  }
}
