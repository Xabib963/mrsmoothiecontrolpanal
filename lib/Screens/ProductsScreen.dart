import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:mrsmoothiespanel/Screens/AddProductScreen.dart';
import 'package:mrsmoothiespanel/Screens/widgets/CustomAppbar.dart';
import 'package:mrsmoothiespanel/app/controller/ProductController.dart';
import 'package:mrsmoothiespanel/main.dart';
import 'package:mrsmoothiespanel/data/model/Product.dart';

import 'widgets/Custombutton.dart';

class ProductsSCreen extends StatelessWidget {
  ProductsSCreen({super.key});
  static const routeName = "/productScreen";
  ProductController controller = Get.put(ProductController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: CustomAppBar(
        context: context,
        allow: true,
        title: "Products",
      ),
      body: SafeArea(
          child: Column(
        children: [
          CustomButtom(
            msg: "Add New Product",
            show: true,
          ),
          Expanded(
            flex: 5,
            child: Obx(() {
              return Padding(
                  padding: const EdgeInsets.all(8),
                  child: controller.products.isNotEmpty
                      ? ListView.builder(
                          itemCount: controller.products.length,
                          itemBuilder: (context, index) => Card(
                                color: Colors.white,
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        "${controller.products[index].name} ",
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyLarge!
                                            .copyWith(fontSize: 25),
                                      ),
                                      SizedBox(
                                        height: medias(context).height * 0.0151,
                                      ),
                                      Text(
                                        maxLines: 3,
                                        overflow: TextOverflow.ellipsis,
                                        controller.products[index].description,
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyMedium!
                                            .copyWith(
                                                fontSize: 16,
                                                color: Colors.black,
                                                fontWeight: FontWeight.w600),
                                      ),
                                      SizedBox(
                                        height: medias(context).height * 0.021,
                                      ),
                                      Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            ClipRRect(
                                              borderRadius:
                                                  BorderRadius.circular(25),
                                              child: Image.network(
                                                fit: BoxFit.fill,
                                                controller
                                                    .products[index].imgUrl,
                                                errorBuilder: (context, error,
                                                        stackTrace) =>
                                                    Container(
                                                        width: 100,
                                                        height: 100,
                                                        alignment:
                                                            Alignment.center,
                                                        child: const Icon(
                                                            Icons.error_outline,
                                                            color: Colors.red)),
                                                width: medias(context).width *
                                                    0.282,
                                                height: medias(context).width *
                                                    0.22,
                                              ),
                                            ),
                                            Column(
                                              children: [
                                                SizedBox(
                                                  width: medias(context).width *
                                                      0.6,
                                                  child: Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceAround,
                                                    children: [
                                                      Expanded(
                                                        flex: 3,
                                                        child: FittedBox(
                                                          child: Text(
                                                            "Price : ",
                                                            maxLines: 1,
                                                            style: Theme.of(
                                                                    context)
                                                                .textTheme
                                                                .bodyLarge!
                                                                .copyWith(
                                                                    fontSize:
                                                                        17),
                                                          ),
                                                        ),
                                                      ),
                                                      SizedBox(
                                                        width: medias(context)
                                                                .width *
                                                            0.32,
                                                        child: GetBuilder<
                                                                ProductController>(
                                                            builder: (_) {
                                                          return Slider(
                                                              activeColor: Colors
                                                                  .deepOrange,
                                                              divisions: 10,
                                                              max: 100.0,
                                                              min: 0.0,
                                                              inactiveColor:
                                                                  Colors.grey,
                                                              value: controller
                                                                  .products[
                                                                      index]
                                                                  .price
                                                                  .toDouble(),
                                                              onChanged:
                                                                  (value) {
                                                                controller.updatePrice(
                                                                    index,
                                                                    controller
                                                                            .products[
                                                                        index],
                                                                    value);
                                                              });
                                                        }),
                                                      ),
                                                      Expanded(
                                                        flex: 2,
                                                        child: FittedBox(
                                                          child: Text(
                                                            maxLines: 1,
                                                            "${controller.products[index].price}\$",
                                                            style: Theme.of(
                                                                    context)
                                                                .textTheme
                                                                .bodyLarge!
                                                                .copyWith(
                                                                    fontSize:
                                                                        14),
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                                SizedBox(
                                                    width:
                                                        medias(context).width *
                                                            0.6,
                                                    child: Row(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .center,
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .spaceBetween,
                                                      children: [
                                                        // SizedBox(
                                                        //   height: medias(context).height * 0.02,
                                                        // ),
                                                        Expanded(
                                                          flex: 3,
                                                          child: FittedBox(
                                                            child: Text(
                                                              maxLines: 1,
                                                              "QTY : ",
                                                              style: Theme.of(
                                                                      context)
                                                                  .textTheme
                                                                  .bodyLarge!
                                                                  .copyWith(
                                                                      fontSize:
                                                                          12),
                                                            ),
                                                          ),
                                                        ),
                                                        SizedBox(
                                                          width: medias(context)
                                                                  .width *
                                                              0.32,
                                                          child: GetBuilder<
                                                                  ProductController>(
                                                              builder: (_) {
                                                            return Slider(
                                                                activeColor: Colors
                                                                    .deepOrange,
                                                                divisions: 10,
                                                                max: 100,
                                                                min: 0,
                                                                inactiveColor:
                                                                    Colors.grey,
                                                                value: controller
                                                                    .products[
                                                                        index]
                                                                    .quantity
                                                                    .toDouble(),
                                                                onChanged:
                                                                    (value) {
                                                                  controller.updateQuantity(
                                                                      index,
                                                                      controller
                                                                              .products[
                                                                          index],
                                                                      value);
                                                                });
                                                          }),
                                                        ),
                                                        Expanded(
                                                          flex: 2,
                                                          child: FittedBox(
                                                            child: Text(
                                                              maxLines: 1,
                                                              "${controller.products[index].quantity}",
                                                              style: Theme.of(
                                                                      context)
                                                                  .textTheme
                                                                  .bodyLarge!
                                                                  .copyWith(
                                                                      fontSize:
                                                                          14),
                                                            ),
                                                          ),
                                                        ),
                                                      ],
                                                    )),
                                              ],
                                            ),
                                          ]),
                                    ],
                                  ),
                                ),
                              ))
                      : const SpinKitFadingFour(
                          color: Colors.deepOrange,
                        ));
            }),
          ),
        ],
      )),
    );
  }
}
