import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:mrsmoothiespanel/Screens/widgets/Custominput.dart';
import 'package:mrsmoothiespanel/Screens/widgets/SliderWidget.dart';
import 'package:mrsmoothiespanel/Screens/widgets/SwitchMaster.dart';
import 'package:mrsmoothiespanel/app/controller/ProductController.dart';
import 'package:mrsmoothiespanel/app/theme.dart';
import 'package:mrsmoothiespanel/main.dart';
import 'package:mrsmoothiespanel/data/model/Product.dart';
import 'package:mrsmoothiespanel/data/services/data_source.dart';

import 'widgets/CustomAppbar.dart';
import 'widgets/Custombutton.dart';

class AddProductScreen extends StatelessWidget {
  AddProductScreen({super.key});
  static const routeName = '/addProduct';
  TextEditingController controller = TextEditingController();
  ProductController productControllor = ProductController();
  DatabaseServices databaseServices = DatabaseServices();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        context: context,
        allow: true,
        title: "Add Product",
      ),
      body: SafeArea(
          child: Column(
        children: [
          CustomButtom(
            productController: productControllor,
            show: false,
            msg: "Add Image :",
          ),
          Expanded(
            flex: 5,
            child: Padding(
              padding: const EdgeInsets.all(15.0),
              child: SingleChildScrollView(
                child: Column(children: [
                  CustomInput(
                      hint: "Product ID",
                      title: "id",
                      control: productControllor),
                  SizedBox(
                    height: medias(context).height * 0.01,
                  ),
                  CustomInput(
                      hint: "Product Name",
                      title: "name",
                      control: productControllor),
                  SizedBox(
                    height: medias(context).height * 0.01,
                  ),
                  CustomInput(
                      hint: " Prduct Description",
                      title: "description",
                      control: productControllor),
                  GetBuilder<ProductController>(builder: (c) {
                    return DropdownButton(
                      style: textTheme()
                          .bodyMedium!
                          .copyWith(color: Colors.black54),
                      value: c.category,
                      alignment: AlignmentDirectional.centerStart,
                      hint: const Text(
                        'Choose Category',
                      ),
                      items: [
                        DropdownMenuItem(
                          value: 'Water',
                          child: Text(
                            'Water',
                            style: textTheme()
                                .bodyMedium!
                                .copyWith(color: Colors.black54, fontSize: 20),
                            textAlign: TextAlign.center,
                          ),
                        ),
                        DropdownMenuItem(
                          value: 'Soft drinks',
                          child: Text('Soft drinks',
                              style: textTheme().bodyMedium!.copyWith(
                                  color: Colors.black54, fontSize: 20),
                              textAlign: TextAlign.center),
                        ),
                        DropdownMenuItem(
                          value: 'smoothies',
                          child: Text('smoothies',
                              style: textTheme().bodyMedium!.copyWith(
                                  color: Colors.black54, fontSize: 20),
                              textAlign: TextAlign.center),
                        ),
                      ],
                      onChanged: (value) {
                        c.updateCategory(value);
                      },
                    );
                  }),
                  SizedBox(
                    height: medias(context).height * 0.01,
                  ),
                  Obx(() {
                    return SliderWidget(
                      title: "price",
                      productControllor: productControllor,
                      value: productControllor.price,
                    );
                  }),
                  Obx(() {
                    return SliderWidget(
                      title: "quantity",
                      productControllor: productControllor,
                      value: productControllor.quantity,
                    );
                  }),
                  SizedBox(
                    height: medias(context).height * 0.01,
                  ),
                  SwitchMaster(
                    tile: "isrecommended",
                    productControllor: productControllor,
                  ),
                  SwitchMaster(
                    tile: "istrendy",
                    productControllor: productControllor,
                  ),
                  InkWell(
                    onTap: () {
                      print(productControllor.newProduct);

                      databaseServices.addProduct({
                        'id': productControllor.newProduct['id'],
                        'name': productControllor.newProduct['name'],
                        'description':
                            productControllor.newProduct['description'],
                        'category': productControllor.newProduct['category'],
                        'price': productControllor.newProduct['price'],
                        'quantity': productControllor.newProduct['quantity'],
                        'isrecommended':
                            productControllor.newProduct['isrecommended'],
                        'istrendy': productControllor.newProduct['istrendy'],
                        'imgUrl': productControllor.newProduct['imgUrl']
                      }).then((value) => Get.snackbar(
                          "added Sucsesfully", "added Sucsesfully"));
                    },
                    child: Container(
                        margin: const EdgeInsets.symmetric(vertical: 30),
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                            color: Colors.deepOrange,
                            borderRadius: BorderRadius.circular(15)),
                        width: medias(context).width * 0.4,
                        height: medias(context).width * 0.12,
                        child: Text(
                          "Send",
                          style: Theme.of(context)
                              .textTheme
                              .bodyLarge!
                              .copyWith(color: Colors.white, fontSize: 30),
                        )),
                  )
                ]),
              ),
            ),
          ),
        ],
      )),
    );
  }
}
