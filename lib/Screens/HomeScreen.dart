import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';

import 'package:mrsmoothiespanel/Screens/OrdersScreen.dart';
import 'package:mrsmoothiespanel/Screens/ProductsScreen.dart';
import 'package:mrsmoothiespanel/Screens/widgets/CateCard.dart';
import 'package:mrsmoothiespanel/Screens/widgets/Charts.dart';
import 'package:mrsmoothiespanel/Screens/widgets/CustomAppbar.dart';
import 'package:mrsmoothiespanel/app/controller/OrderStampsController.dart';
import 'package:mrsmoothiespanel/main.dart';

class HomePage extends StatelessWidget {
  HomePage({
    super.key,
  });
  static const routeName = '/';
  OrderStampController stampController = Get.put(OrderStampController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(title: "Controll", context: context, allow: false),
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Container(
                padding: const EdgeInsets.all(20),
                height: medias(context).height * 0.33,
                child: FutureBuilder(
                    future: stampController.stamps.value,
                    builder: (context, snap) {
                      return snap.hasData
                          ? Charts(stamps: snap.data!)
                          : const SpinKitFadingFour(
                              color: Colors.deepOrange,
                            );
                    })),
            CateCard(
              title: "Products",
              co: 1,
              fun: () {
                Get.toNamed(ProductsSCreen.routeName);
              },
            ),
            CateCard(
              co: 0,
              title: "Orders",
              fun: () {
                Get.toNamed(OrdersSCreen.routeName);
              },
            )
          ],
        ),
      ),
    );
  }
}
