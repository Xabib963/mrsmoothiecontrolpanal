import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:intl/intl.dart';
import 'package:mrsmoothiespanel/main.dart';
import 'package:mrsmoothiespanel/data/model/Order_stamps.dart';

class Charts extends StatelessWidget {
  const Charts({super.key, required this.stamps});
  final List<OrderStamps> stamps;
  @override
  Widget build(BuildContext context) {
    List<charts.Series<OrderStamps, String>> series = [
      charts.Series(
        id: 'order',
        data: stamps,
        domainFn: (serie, _) =>
            DateFormat.d().format(serie.dateTime).toString(),
        measureFn: (serie, _) => serie.orders,
        colorFn: (serie, _) => serie.barColor!,
      )
    ];
    return charts.BarChart(
      series,
      animate: true,
    );
  }
}
