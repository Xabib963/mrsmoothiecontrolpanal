import 'package:flutter/material.dart';
import 'package:get/get_state_manager/src/rx_flutter/rx_obx_widget.dart';

import '../../app/controller/ProductController.dart';
import '../../main.dart';

class SwitchMaster extends StatelessWidget {
  const SwitchMaster({
    super.key,
    required this.tile,
    required this.productControllor,
  });
  final String tile;
  final ProductController productControllor;
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: medias(context).width * 0.5,
      child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
        Text(
          tile,
          style: Theme.of(context).textTheme.bodyLarge!.copyWith(fontSize: 18),
        ),
        Obx(() {
          return Switch(
            activeColor: Colors.deepOrange,
            value: productControllor.newProduct[tile] ?? false,
            onChanged: (value) {
              productControllor.newProduct
                  .update(tile, (_) => value, ifAbsent: () => value);
            },
          );
        })
      ]),
    );
  }
}
