import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:mrsmoothiespanel/app/controller/ProductController.dart';
import 'package:mrsmoothiespanel/data/services/StorageSevices.dart';
import 'package:get/get.dart';
import '../../main.dart';
import '../AddProductScreen.dart';

class CustomButtom extends StatelessWidget {
  CustomButtom({
    super.key,
    required this.msg,
    required this.show,
    this.productController,
  });
  final String msg;
  final bool show;
  final ProductController? productController;
  ImagePicker picker = ImagePicker();
  XFile? image;
  StorageService storageService = StorageService();

  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: 1,
      child: Container(
        width: double.infinity,
        margin: EdgeInsets.symmetric(
            horizontal: medias(context).width * 0.05,
            vertical: medias(context).height * 0.01),
        child: InkWell(
          onTap: show
              ? () {
                  Get.toNamed(AddProductScreen.routeName);
                }
              : () async {
                  image = await picker.pickImage(source: ImageSource.gallery);
                  if (image == null) {}
                  if (image != null) {
                    Get.snackbar(
                      "please Wait",
                      'we are uploading the image',
                    );
                    await storageService.uploadImage(image);
                    Get.snackbar(
                      "Keep Waiting",
                      '',
                    );
                    String imageURl =
                        await storageService.getDownloadUrl(image!.name);
                    productController!.newProduct.update(
                      'imgUrl',
                      (_) => imageURl,
                      ifAbsent: () => imageURl,
                    );
                    Get.snackbar(
                        "upload Done", 'You can now press the button send');
                  }
                },
          child: Card(
            color: Colors.deepOrange,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 22.0),
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    SizedBox(
                        width: medias(context).width * 0.6,
                        child: FittedBox(
                          child: Text(
                            msg,
                            maxLines: 1,
                            style: Theme.of(context)
                                .textTheme
                                .headlineLarge!
                                .copyWith(fontSize: 28),
                          ),
                        )),
                    SizedBox(
                        width: medias(context).width * 0.1,
                        child: Icon(
                          Icons.add_circle,
                          color: Colors.white,
                          size: medias(context).width * 0.08,
                        ))
                  ]),
            ),
          ),
        ),
      ),
    );
  }
}
