import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../main.dart';

class CustomAppBar extends StatelessWidget with PreferredSizeWidget {
  const CustomAppBar(
      {super.key,
      required this.title,
      required this.context,
      required this.allow});
  final String title;
  final bool allow;
  final BuildContext context;
  @override
  Widget build(BuildContext context) {
    return AppBar(
      shadowColor: Colors.white38,
      scrolledUnderElevation: 0.19,
      automaticallyImplyLeading: false,
      title: Text(
        title,
        style: Theme.of(context)
            .textTheme
            .bodyLarge!
            .copyWith(fontSize: 30, color: Colors.deepOrange),
      ),
      centerTitle: true,
      elevation: 0,
      backgroundColor: Colors.transparent,
      leading: allow
          ? IconButton(
              icon: const Icon(Icons.arrow_back_ios, color: Colors.deepOrange),
              onPressed: () {
                Get.back();
              },
            )
          : const SizedBox(),
    );
  }

  @override
  // TODO: implement preferredSize
  Size get preferredSize => Size.fromHeight(medias(context).height * 0.07);
}
