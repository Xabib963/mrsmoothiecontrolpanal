import 'package:flutter/material.dart';

import '../../main.dart';

class CateCard extends StatelessWidget {
  const CateCard({
    super.key,
    required this.title,
    required this.fun,
    required this.co,
  });
  final int co;
  final String title;
  final void Function() fun;
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: fun,
      child: Card(
        color: co == 1
            ? Colors.deepOrange.withOpacity(0.9)
            : Colors.amber.withOpacity(0.9),
        child: SizedBox(
            height: medias(context).height * 0.15,
            width: medias(context).width,
            child: Stack(
              clipBehavior: Clip.hardEdge,
              children: [
                const Positioned(
                  right: -20,
                  top: 3,
                  child: CircleAvatar(
                    radius: 100,
                    backgroundColor: Colors.white38,
                  ),
                ),
                const Positioned(
                  right: 60,
                  top: 40,
                  child: CircleAvatar(
                    radius: 80,
                    backgroundColor: Colors.white54,
                  ),
                ),
                const Positioned(
                  right: 140,
                  top: 70,
                  child: CircleAvatar(
                    radius: 50,
                    backgroundColor: Colors.white60,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: SizedBox(
                    width: medias(context).width * 0.35,
                    child: FittedBox(
                      child: Text(
                        maxLines: 1,
                        title,
                        style: Theme.of(context)
                            .textTheme
                            .headlineLarge!
                            .copyWith(color: Colors.white),
                      ),
                    ),
                  ),
                ),
              ],
            )),
      ),
    );
  }
}
