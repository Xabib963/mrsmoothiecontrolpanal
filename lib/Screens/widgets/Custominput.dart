import 'package:flutter/material.dart';
import 'package:mrsmoothiespanel/app/controller/ProductController.dart';

import '../../main.dart';

class CustomInput extends StatelessWidget {
  const CustomInput({
    super.key,
    required this.hint,
    required this.title,
    required this.control,
  });
  final String hint;
  final String title;
  final ProductController control;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: medias(context).width * 0.9,
      height: medias(context).height * 0.061,
      child: TextFormField(
        onChanged: (value) => control.newProduct
            .update(title, (_) => value, ifAbsent: () => title),
        textAlign: TextAlign.start,
        textAlignVertical: TextAlignVertical.center,
        decoration: InputDecoration(
          hintText: hint,
          alignLabelWithHint: true,
          focusedBorder: const UnderlineInputBorder(),
          // enabledBorder: OutlineInputBorder(
          //     borderSide: BorderSide(color: Colors.black)),
        ),
      ),
    );
  }
}
