// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:intl/intl.dart';
import 'package:mrsmoothiespanel/data/model/checkouts.dart';
import '../../app/controller/OrdersController.dart';
import '../../app/controller/ProductController.dart';
import 'package:mrsmoothiespanel/main.dart';

import '../../data/model/Orders.dart';
import '../../data/model/Product.dart';
import '../../data/services/data_source.dart';

class OrderCard extends StatelessWidget {
  OrderCard({
    Key? key,
    required this.checkouts,
    required this.controlpro,
    required this.chrController,
    required this.index,
  }) : super(key: key);
  final int index;
  final Checkouts checkouts;
  final CheckoutsController chrController;
  final ProductController controlpro;
  DatabaseServices databaseServices = DatabaseServices();
  @override
  Widget build(BuildContext context) {
    return Card(
      color: index % 2 == 1
          ? Colors.amber.withOpacity(0.9)
          : Colors.deepOrange.withOpacity(0.9),
      // shape: RoundedRectangleBorder(
      //     borderRadius: BorderRadius.circular(15),
      //     side: const BorderSide(
      //       color: Colors.deepOrange,
      //       width: 2,
      //     )),
      child: Stack(
        children: [
          const Positioned(
            right: -70,
            top: -50,
            child: CircleAvatar(
              radius: 100,
              backgroundColor: Colors.white38,
            ),
          ),
          const Positioned(
            right: 10,
            bottom: -90,
            child: CircleAvatar(
              radius: 60,
              backgroundColor: Colors.white54,
            ),
          ),
          const Positioned(
            right: 140,
            top: -60,
            child: CircleAvatar(
              radius: 50,
              backgroundColor: Colors.white60,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                SizedBox(
                    child: FutureBuilder(
                        future: databaseServices.getcheproducts(
                            chrController.checkouts[index].products!),
                        builder: (context, snapshot) {
                          return snapshot.hasData
                              ? ListView.builder(
                                  physics: const NeverScrollableScrollPhysics(),
                                  itemCount: snapshot.data!.length,
                                  shrinkWrap: true,
                                  itemBuilder: (context, index) {
                                    return Card(
                                      color: Colors.white.withOpacity(0.9),
                                      child: Padding(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 8.0),
                                        child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              ClipRRect(
                                                borderRadius:
                                                    BorderRadius.circular(10),
                                                child: Image.network(
                                                  snapshot.data![index].imgUrl,
                                                  fit: BoxFit.fill,
                                                  errorBuilder: (context, error,
                                                          stackTrace) =>
                                                      Container(
                                                          width: 100,
                                                          height: 100,
                                                          alignment:
                                                              Alignment.center,
                                                          child: Icon(
                                                              Icons
                                                                  .error_outline,
                                                              color:
                                                                  Colors.red)),
                                                  width: 70,
                                                  height: 50,
                                                ),
                                              ),
                                              SizedBox(
                                                width:
                                                    medias(context).width * 0.7,
                                                height: medias(context).height *
                                                    0.1,
                                                child: Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    Text(
                                                      overflow:
                                                          TextOverflow.ellipsis,
                                                      maxLines: 1,
                                                      textAlign:
                                                          TextAlign.center,
                                                      "${snapshot.data![index].name}  ",
                                                      style: Theme.of(context)
                                                          .textTheme
                                                          .bodyLarge!
                                                          .copyWith(
                                                              fontSize: 20,
                                                              color: Colors
                                                                  .black54),
                                                    ),
                                                    Text(
                                                      overflow:
                                                          TextOverflow.ellipsis,
                                                      maxLines: 3,
                                                      textAlign:
                                                          TextAlign.center,
                                                      " ${snapshot.data![index].description} ",
                                                      style: Theme.of(context)
                                                          .textTheme
                                                          .bodyLarge!
                                                          .copyWith(
                                                              fontSize: 15,
                                                              color: Colors
                                                                  .black54),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ]),
                                      ),
                                    );
                                  },
                                )
                              : const SpinKitFadingFour(
                                  color: Colors.deepOrange,
                                );
                        })),
                Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: SizedBox(
                      width: medias(context).width * 0.9,
                      child: RichText(
                          text: TextSpan(children: [
                        TextSpan(
                          text: "Name : ",
                          style: Theme.of(context)
                              .textTheme
                              .bodyLarge!
                              .copyWith(fontSize: 16, color: Colors.white),
                        ),
                        TextSpan(
                          text: "${checkouts.name}",
                          style: Theme.of(context)
                              .textTheme
                              .bodyLarge!
                              .copyWith(fontSize: 16, color: Colors.black54),
                        )
                      ])),
                    )),
                Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: SizedBox(
                      width: medias(context).width * 0.9,
                      child: RichText(
                          text: TextSpan(children: [
                        TextSpan(
                          text: "Email : ",
                          style: Theme.of(context)
                              .textTheme
                              .bodyLarge!
                              .copyWith(fontSize: 16, color: Colors.white),
                        ),
                        TextSpan(
                          text: "${checkouts.email}",
                          style: Theme.of(context)
                              .textTheme
                              .bodyLarge!
                              .copyWith(fontSize: 16, color: Colors.black54),
                        )
                      ])),
                    )),
                Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: SizedBox(
                            width: medias(context).width * 0.43,
                            child: RichText(
                                text: TextSpan(children: [
                              TextSpan(
                                text: "City : ",
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyLarge!
                                    .copyWith(
                                        fontSize: 16, color: Colors.white),
                              ),
                              TextSpan(
                                text: "${checkouts.city}",
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyLarge!
                                    .copyWith(
                                        fontSize: 16, color: Colors.black54),
                              )
                            ])),
                          )),
                      Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: SizedBox(
                            width: medias(context).width * 0.43,
                            child: RichText(
                                text: TextSpan(children: [
                              TextSpan(
                                text: "Address : ",
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyLarge!
                                    .copyWith(
                                        fontSize: 16, color: Colors.white),
                              ),
                              TextSpan(
                                text: "${checkouts.address}",
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyLarge!
                                    .copyWith(
                                        fontSize: 16, color: Colors.black54),
                              )
                            ])),
                          )),
                    ],
                  ),
                ),
                // Row(
                //   mainAxisAlignment: MainAxisAlignment.spaceAround,
                //   children: [
                //     Expanded(
                //       child: Padding(
                //         padding: const EdgeInsets.symmetric(horizontal: 8.0),
                //         child: ElevatedButton(
                //             style: ElevatedButton.styleFrom(
                //               padding: const EdgeInsets.all(10),
                //               backgroundColor: Colors.black,
                //             ),
                //             onPressed: () {},
                //             child: Text("Accept",
                //                 style: Theme.of(context)
                //                     .textTheme
                //                     .bodyLarge!
                //                     .copyWith(fontSize: 16, color: Colors.white))),
                //       ),
                //     ),
                //     Expanded(
                //       child: Padding(
                //         padding: const EdgeInsets.symmetric(horizontal: 8.0),
                //         child: ElevatedButton(
                //             style: ElevatedButton.styleFrom(
                //                 backgroundColor: Colors.black,
                //                 padding: const EdgeInsets.all(10)),
                //             onPressed: () {},
                //             child: Text("Decline",
                //                 style: Theme.of(context)
                //                     .textTheme
                //                     .bodyLarge!
                //                     .copyWith(fontSize: 16, color: Colors.white))),
                //       ),
                //     )
                //   ],
                // )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
