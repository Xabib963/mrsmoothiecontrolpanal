import 'package:flutter/material.dart';

import '../../app/controller/ProductController.dart';
import '../../main.dart';

class SliderWidget extends StatelessWidget {
  const SliderWidget({
    super.key,
    required this.productControllor,
    required this.value,
    required this.title,
  });

  final ProductController productControllor;
  final double? value;
  final String title;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          "$title :",
          style: Theme.of(context).textTheme.bodyLarge!.copyWith(fontSize: 18),
        ),
        SizedBox(
            width: medias(context).width * 0.62,
            child: Slider(
                label: value == null ? '0' : '$value',
                activeColor: Colors.deepOrange,
                divisions: 10,
                max: 100,
                min: 0,
                inactiveColor: Colors.grey,
                value: value == null
                    ? 0
                    : productControllor.newProduct[title]!.toDouble(),
                onChanged: (value) {
                  productControllor.newProduct.update(
                    title,
                    (_) => value,
                  );
                }))
      ],
    );
  }
}
