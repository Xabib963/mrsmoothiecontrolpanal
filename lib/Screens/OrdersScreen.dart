import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:mrsmoothiespanel/Screens/widgets/CustomAppbar.dart';
import 'package:mrsmoothiespanel/Screens/widgets/OrderCard.dart';
import 'package:mrsmoothiespanel/app/controller/ProductController.dart';
import 'package:mrsmoothiespanel/data/model/Orders.dart';
import 'package:mrsmoothiespanel/data/model/Product.dart';
import 'package:mrsmoothiespanel/data/model/checkouts.dart';

import '../app/controller/OrdersController.dart';

class OrdersSCreen extends StatelessWidget {
  OrdersSCreen({super.key});
  static const routeName = '/orderScreen';
  CheckoutsController controller = Get.put(CheckoutsController());
  ProductController prcontroller = Get.put(ProductController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(title: "Orders", context: context, allow: true),
      body: SafeArea(
          child: Column(
        children: [
          Expanded(
              flex: 4,
              child: GetBuilder<CheckoutsController>(builder: (c) {
                return ListView.builder(
                    shrinkWrap: true,
                    itemBuilder: (context, index) => OrderCard(
                          index: index,
                          checkouts: controller.checkouts[index],
                          controlpro: prcontroller,
                          chrController: controller,
                        ),
                    itemCount: controller.checkouts.length);
              })),
        ],
      )),
    );
  }
}
