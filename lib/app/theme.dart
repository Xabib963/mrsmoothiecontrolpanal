import 'package:flutter/material.dart';

ThemeData theme() {
  return ThemeData(
      colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepOrange),
      useMaterial3: false,
      primaryColor: Colors.deepOrange,
      primarySwatch: Colors.deepOrange,
      canvasColor: Colors.white,
      fontFamily: 'Avenir',
      textTheme: textTheme());
}

TextTheme textTheme() {
  return TextTheme(
      bodyLarge:
          TextStyle(color: Colors.grey[700], fontWeight: FontWeight.bold),
      bodyMedium:
          TextStyle(color: Colors.grey[700], fontWeight: FontWeight.normal),
      bodySmall:
          const TextStyle(color: Colors.white, fontWeight: FontWeight.normal),
      headlineLarge:
          const TextStyle(color: Colors.white, fontWeight: FontWeight.bold));
}
