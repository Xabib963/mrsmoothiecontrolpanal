import 'package:get/get.dart';
import 'package:mrsmoothiespanel/Screens/AddProductScreen.dart';
import 'package:mrsmoothiespanel/Screens/HomeScreen.dart';
import 'package:mrsmoothiespanel/Screens/OrdersScreen.dart';
import 'package:mrsmoothiespanel/Screens/ProductsScreen.dart';

appRoutes() => [
      GetPage(
        name: HomePage.routeName,
        page: () => HomePage(),
        // binding: CheckoutBinding(),
        middlewares: [MyMiddelware()],
        transition: Transition.circularReveal,
        transitionDuration: Duration(milliseconds: 500),
      ),
      GetPage(
        name: ProductsSCreen.routeName,
        page: () => ProductsSCreen(),
        // binding: CheckoutBinding(),
        middlewares: [MyMiddelware()],
        transition: Transition.circularReveal,
        transitionDuration: Duration(milliseconds: 500),
      ),
      GetPage(
        name: AddProductScreen.routeName,
        page: () => AddProductScreen(),
        // binding: CheckoutBinding(),
        middlewares: [MyMiddelware()],
        transition: Transition.circularReveal,
        transitionDuration: Duration(milliseconds: 500),
      ),
      GetPage(
        name: OrdersSCreen.routeName,
        page: () => OrdersSCreen(),
        // binding: CheckoutBinding(),
        middlewares: [MyMiddelware()],
        transition: Transition.circularReveal,
        transitionDuration: Duration(milliseconds: 500),
      ),
    ];

class MyMiddelware extends GetMiddleware {
  @override
  GetPage? onPageCalled(GetPage? page) {
    print(page?.name);
    return super.onPageCalled(page);
  }
}
