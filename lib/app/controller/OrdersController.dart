import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mrsmoothiespanel/app/controller/ProductController.dart';
import 'package:mrsmoothiespanel/data/model/Orders.dart';
import 'package:mrsmoothiespanel/data/model/Product.dart';
import 'package:mrsmoothiespanel/data/model/checkouts.dart';

import '../../data/services/data_source.dart';

class CheckoutsController extends GetxController {
  DatabaseServices databaseServices = DatabaseServices();
  List<Checkouts> checkouts = [];

  @override
  void onInit() async {
    checkouts = await databaseServices.getcheckouts();
    update();
    super.onInit();
  }
}
