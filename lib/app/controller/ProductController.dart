import 'package:get/get.dart';
import 'package:mrsmoothiespanel/data/model/Product.dart';
import 'package:mrsmoothiespanel/data/model/checkouts.dart';

import '../../data/services/data_source.dart';

class ProductController extends GetxController {
  DatabaseServices databaseServices = DatabaseServices();
  var products = <Product>[].obs;
  String category = 'smoothies';
  @override
  void onInit() async {
    products.bindStream(databaseServices.getProducts());

    // products = await databaseServices.getproducts();
    print(products);
    update();
    super.onInit();
  }

  RxMap newProduct = {
    'id': '00',
    'name': 'name',
    'description': 'description',
    'category': 'category',
    'price': 2.2,
    'quantity': 2.0,
    'isrecommended': false,
    'istrendy': false,
    'imgUrl': 'https://cdn.icon-icons.com/icons2/483/PNG/512/image_47199.png'
  }.obs;
  void updateCategory(value) {
    print('object');
    category = value;
    newProduct.update('category', (_) => category, ifAbsent: () => 'category');
    update();
  }

  get price => newProduct['price'];
  get quantity => newProduct['quantity'];

  void updatePrice(int index, Product pro, double value) {
    pro.price = value;
    products[index] = pro;
    update();
  }

  void updateQuantity(int index, Product pro, double value) {
    pro.quantity = value;
    products[index] = pro;
  }
}
