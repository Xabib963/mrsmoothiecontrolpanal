import 'package:get/get.dart';
import 'package:mrsmoothiespanel/data/model/Order_stamps.dart';
import 'package:mrsmoothiespanel/data/model/Product.dart';

import '../../data/services/data_source.dart';

class OrderStampController extends GetxController {
  DatabaseServices databaseServices = DatabaseServices();
  var stamps = Future.value(<OrderStamps>[]).obs;
  @override
  void onInit() {
    stamps.value = databaseServices.getStamps();
    super.onInit();
  }
}
