import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';

class Product extends Equatable {
  final String name;
  final String imgUrl;
  final String category;
  final String description;
  final String id;
  double price;
  double quantity;
  final bool isrecommended;
  final bool istrendy;
  Product(
      {required this.quantity,
      required this.isrecommended,
      required this.category,
      required this.istrendy,
      required this.id,
      required this.name,
      required this.imgUrl,
      required this.price,
      required this.description});

  @override
  // TODO: implement props
  List<Object?> get props =>
      [name, imgUrl, quantity, isrecommended, istrendy, price, id];

  factory Product.fromSnap(Map<String, dynamic> doc) {
    return Product(
        category: doc['category'] as String,
        quantity: doc['quantity'],
        isrecommended: doc['isrecommended'],
        istrendy: doc["istrendy"],
        id: doc['id'] as String,
        name: doc['name'] as String,
        imgUrl: doc['imgUrl'] as String,
        price: doc['price'],
        description: doc['description'] as String);
  }
  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
      'quantity': quantity,
      'price': price,
      'imgUrl': imgUrl,
      "category": category,
      'isrecommended': isrecommended,
      'istrendy': istrendy,
      'description': description
    };
  }

  static List<Product> prods = [
    Product(
        category: 'soft drinks',
        description:
            "Ea enim tempor qui fugiat nostrud. Officia nostrud culpa irure laboris sint laborum reprehenderit eu et aliqua culpa. Sunt reprehenderit amet sint ad est ullamco reprehenderit eu non. Ad amet incididunt consectetur consectetur. Aliqua pariatur sit aliqua amet minim do aliqua commodo.",
        isrecommended: true,
        istrendy: false,
        quantity: 0,
        price: 10,
        name: 'Water',
        id: '1',
        imgUrl:
            'https://images.unsplash.com/photo-1520342868574-5fa3804e551c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=6ff92caffcdd63681a35134a6770ed3b&auto=format&fit=crop&w=1951&q=80'),
    Product(
        category: 'soft drinks',
        description:
            "Ea enim tempor qui fugiat nostrud. Officia nostrud culpa irure laboris sint laborum reprehenderit eu et aliqua culpa. Sunt reprehenderit amet sint ad est ullamco reprehenderit eu non. Ad amet incididunt consectetur consectetur. Aliqua pariatur sit aliqua amet minim do aliqua commodo.",
        isrecommended: true,
        istrendy: false,
        quantity: 0,
        price: 10,
        name: 'csa',
        id: '2',
        imgUrl:
            'https://images.unsplash.com/photo-1520342868574-5fa3804e551c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=6ff92caffcdd63681a35134a6770ed3b&auto=format&fit=crop&w=1951&q=80'),
    Product(
        category: 'soft drinks',
        description:
            "Ea enim tempor qui fugiat nostrud. Officia nostrud culpa irure laboris sint laborum reprehenderit eu et aliqua culpa. Sunt reprehenderit amet sint ad est ullamco reprehenderit eu non. Ad amet incididunt consectetur consectetur. Aliqua pariatur sit aliqua amet minim do aliqua commodo.",
        isrecommended: true,
        istrendy: false,
        quantity: 0,
        price: 10,
        name: 'ass',
        id: '1',
        imgUrl:
            'https://images.unsplash.com/photo-1520342868574-5fa3804e551c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=6ff92caffcdd63681a35134a6770ed3b&auto=format&fit=crop&w=1951&q=80'),
  ];
}

// class Product {
//   int? id;
//   String? name;
//   double? quantity;
//   double? price;
//   String? imgUrl;
//   bool? isrecommended;
//   bool? istrendy;
//   String? description;

//   Product(
//       {this.id,
//       this.name,
//       this.quantity,
//       this.price,
//       this.imgUrl,
//       this.isrecommended,
//       this.istrendy,
//       this.description});

//   Product.fromJson(DocumentSnapshot json) {
//     id = json['id'];
//     name = json['name'];
//     quantity = json['quantity'];
//     price = json['price'];
//     imgUrl = json['imgUrl'];
//     isrecommended = json['isrecommended'];
//     istrendy = json['istrendy'];
//     description = json['description'];
//   }

//   Map<String, dynamic> toJson() {
//       return {
//       'id': id,
//       'name': name,
//       'quantity': quantity,
//       'price': price,
//       'imgUrl': imgUrl,
//       'isrecommended': isrecommended,
//       'istrendy': istrendy,
//       'description': description
//     };
//   }


// }
