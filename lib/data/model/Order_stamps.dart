import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';

import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';

class OrderStamps {
  final int index;
  final int orders;
  final DateTime dateTime;
  charts.Color? barColor;

  OrderStamps(this.index, this.orders, this.dateTime) {
    barColor = charts.ColorUtil.fromDartColor(Colors.deepOrange);
  }
  factory OrderStamps.fromSnap(DocumentSnapshot snap, int index) {
    return OrderStamps(index, snap['orders'], snap['dateTime'].toDate());
  }
  // static List<OrderStamps> stamps = [
  //   OrderStamps(0, 12, DateTime.now()),
  //   OrderStamps(1, 100, DateTime.now()),
  //   OrderStamps(3, 50, DateTime.now())
  // ];
}
