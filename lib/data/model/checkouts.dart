import 'package:cloud_firestore/cloud_firestore.dart';

class Checkouts {
  String? address;
  String? city;
  String? email;
  String? name;
  List<String>? products;

  Checkouts({this.address, this.city, this.email, this.name, this.products});

  factory Checkouts.fromJson(QueryDocumentSnapshot json) {
    return Checkouts(
        address: json['address'],
        city: json['city'],
        email: json['email'],
        name: json['name'],
        products: json['products'].cast<String>());
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['address'] = this.address;
    data['city'] = this.city;
    data['email'] = this.email;
    data['name'] = this.name;
    data['products'] = this.products;
    return data;
  }
}
