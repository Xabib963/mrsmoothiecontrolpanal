import 'package:cloud_firestore/cloud_firestore.dart';

class Orders {
  int? id;
  int? customerId;
  List<int>? productIds;
  double? deliveryfee;
  double? subtotal;
  double? total;
  bool? isAccepted;
  bool? isDeliverd;
  String? createdAt;

  Orders(
      {required this.id,
      required this.customerId,
      required this.productIds,
      required this.deliveryfee,
      required this.subtotal,
      required this.total,
      required this.isAccepted,
      required this.isDeliverd,
      required this.createdAt});

  Orders.fromSnap(DocumentSnapshot json) {
    id = json['id'];
    customerId = json['customerId'];
    productIds = json['productIds'].cast<int>();
    deliveryfee = json['deliveryfee'];
    subtotal = json['subtotal'];
    total = json['total'];
    isAccepted = json['isAccepted'];
    isDeliverd = json['isDeliverd'];
    createdAt = json['createdAt'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['customerId'] = this.customerId;
    data['productIds'] = this.productIds;
    data['deliveryfee'] = this.deliveryfee;
    data['subtotal'] = this.subtotal;
    data['total'] = this.total;
    data['isAccepted'] = this.isAccepted;
    data['isDeliverd'] = this.isDeliverd;
    data['createdAt'] = this.createdAt;
    return data;
  }

  // static List<Orders> orders = [
  //   Orders(
  //       id: 1,
  //       customerId: 2,
  //       productIds: [1, 2],
  //       deliveryfee: 200.1,
  //       subtotal: 500.2,
  //       total: 100.1,
  //       isAccepted: true,
  //       isDeliverd: false,
  //       createdAt: DateTime(2023, 3, 2)),
  //   Orders(
  //       id: 2,
  //       customerId: 4,
  //       productIds: [5, 2],
  //       deliveryfee: 200.1,
  //       subtotal: 500.2,
  //       total: 1000.1,
  //       isAccepted: false,
  //       isDeliverd: false,
  //       createdAt: DateTime(2023, 1, 3)),
  //   Orders(
  //       id: 4,
  //       customerId: 8,
  //       productIds: [6, 5],
  //       deliveryfee: 2020.1,
  //       subtotal: 5050.2,
  //       total: 10500.1,
  //       isAccepted: true,
  //       isDeliverd: true,
  //       createdAt: DateTime(2023, 2, 3))
  // ];
}
