import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:mrsmoothiespanel/data/model/Order_stamps.dart';
import '../model/Product.dart';
import '../model/Orders.dart';
import '../model/checkouts.dart';

class DatabaseServices {
  final FirebaseFirestore firebaseFirestore = FirebaseFirestore.instance;
  Stream<List<Product>> getProducts() {
    Stream<QuerySnapshot<Map<String, dynamic>>> data =
        firebaseFirestore.collection('products').snapshots();
    // print(data.first.then((value) => print(value.docs.first['istrendy'])));
    // List<Product> products = data.docs.map((e) => Product.fromSnap(e)).toList();
    // print(data.docs.first['name']);
    return data
        .map((e1) => e1.docs.map((e) => Product.fromSnap(e.data())).toList());
    // print(data.docs.first['name']);
    // return firebaseFirestore
    //     .collection('products')
    //     .snapshots()
    //     .map((snapshots) => snapshots.docs.map((e) {
    //           print(e);
    //           return Product.fromSnap(e);
    //         }).toList());
  }

  Future<List<OrderStamps>> getStamps() {
    return firebaseFirestore.collection('stats').orderBy('dateTime').get().then(
        (value) => value.docs
            .asMap()
            .entries
            .map((e) => OrderStamps.fromSnap(e.value, e.key))
            .toList());
  }

  Future<List<Checkouts>> getcheckouts() async {
    QuerySnapshot<Map<String, dynamic>> data =
        await firebaseFirestore.collection('checkouts').get();
    var data1 = data.docs.map((e) => Checkouts.fromJson(e)).toList();

    return data1;
  }

  Future<List<Product>> getcheproducts(List<String> pro) async {
    QuerySnapshot<Map<String, dynamic>> data = await firebaseFirestore
        .collection('products')
        .where("name", whereIn: pro)
        .get();
    var data1 = data.docs.map((e) => Product.fromSnap(e.data())).toList();

    return data1;
  }

  // Future<List<Product>> getproducts() async {
  //   var data1 = await firebaseFirestore
  //       .collection('products')
  //       .get()
  //       .then((value) => value.docs.map((e) {
  //             print(e.data());
  //             return Product.fromSnap(e.data());
  //           }).toList());

  //   print(data1);
  //   return data1;
  // }

  Future addProduct(pro) async {
    return firebaseFirestore.collection('products').add(pro);
  }
}
